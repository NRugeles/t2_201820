package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<E> extends Iterable<E> {

	 Integer getSize();
	 void agregarElemento(E elem);
	 void agregarAtk(int pos, E elem);
	 void eliminarAtk(int pos,E elem);
	 void eliminarElemento(E elem);
	 void iniciar();
	 E recuperarElem(int pos);
	 E darElemento();
	 void avanzar();
	 void retroceder();
	 boolean isEmpty();
	 

}
