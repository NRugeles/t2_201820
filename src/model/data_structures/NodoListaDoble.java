package model.data_structures;

public class NodoListaDoble<E>{
	
	private E elemento;
	
	private NodoListaDoble<E> anterior; 
	
	private NodoListaDoble<E> siguiente;
	/**
	 * Método constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenará en el nodo.
	 */
	public NodoListaDoble(E elemento) 
	{
		//TODO Completar de acuerdo a la documentación.
		this.elemento=elemento;
		siguiente = null;
		anterior=null;
	}
	
	/**
	 * Método que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public NodoListaDoble<E> darAnterior()
	{
		//TODO Completar de acuerdo a la documentación.
		return anterior;
	}
	/**
	 * Método que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public NodoListaDoble<E> darSiguiente()
	{
		//TODO Completar de acuerdo a la documentación.
		return siguiente;
	}
	
	/**
	 * Método que cambia el nodo anterior por el que llega como parámetro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(NodoListaDoble<E> anterior)
	{
		//TODO Completar de acuerdo a la documentación.
		this.anterior=anterior;
	}
	/**
	 * Método que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(NodoListaDoble<E> siguiente)
	{
		//TODO Completar de acuerdo a la documentación.
		this.siguiente=siguiente;
	}
	
	/**
	 * Método que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{
		//TODO Completar de acuerdo a la documentación.
		return elemento;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenará en el nodo.
	 */
	public void cambiarElemento(E elemento)
	{
		//TODO Completar de acuerdo a la documentación.
		this.elemento =elemento;
	}
}
